<?php
/**
 * xushuhui
 * 2017/10/17
 * 16:40
 */

namespace App;
use \Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
	protected $guarded = [];//不可以注入的字段
}