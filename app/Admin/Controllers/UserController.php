<?php
/**
 * Created by PhpStorm.
 * User: xushuhui
 * Date: 2017/12/11
 * Time: 15:13
 */

namespace App\Admin\Controllers;


use App\AdminUser;

class UserController extends Controller
{
    //管理员列表
    public function index()
    {
        $users = AdminUser::paginate(10);
        return view('/admin/user/index',compact('users'));
    }
    //管理员创建页面
    public function create()
    {
        return view('/admin/user/create');
    }
    //创建用户行为
    public function store()
    {
        $this->validate(request(),[
           'name' => 'required|min:3',
            'password'=>'required|min:3'
        ]);
        $name = request('name');
        $password = bcrypt(request('password'));
        AdminUser::create(compact('name','password'));
        return redirect('/admin/users');
    }
}