<?php
/**
 * Created by PhpStorm.
 * User: xushuhui
 * Date: 2017/12/11
 * Time: 15:13
 */

namespace App\Admin\Controllers;


use App\AdminUser;

class TopicController extends Controller
{
    //专题列表
    public function index()
    {
        $topics = \App\Topic::all();
        return view('/admin/topic/index',compact('topics'));
    }
    //专题创建页面
    public function create()
    {
        return view('/admin/topic/create');
    }
     //专题创建
    public function store()
    {
       $this->validate(request(),['name' => 'required|string']);
       \App\Topic::create(['name' => request('name')]);
       return redirect('/admin/topics');
    }
    //专题删除
    public function destroy(\App\Topic $topic)
    {
       $topic->delete();
       return true;
    }
  
}