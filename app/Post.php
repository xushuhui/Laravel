<?php

namespace App;



class Post extends Model
{
	//protected $fillable = ['title','content']; //可以注入的字段
    //protected $guarded;//不可以注入的字段
    //关联用户
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    //评论模型
    public function comments()
    {
        return $this->hasMany('App\Comment')->orderBy('created_at','desc');
    }
    public function zan($user_id)
    {
        return $this->hasOne('App\Zan')->where('user_id',$user_id);
    }
    //文章所有赞
    public function zans()
    {
        return $this->hasMany('App\Zan');
    }
    //全局scope


}
