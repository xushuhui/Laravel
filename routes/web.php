<?php
//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/','[controller/action]')
//文章列表页
Route::get('/posts','\App\Http\Controllers\PostController@index');
//创建文章
Route::get('/posts/create','\App\Http\Controllers\PostController@create');
Route::post('/posts','\App\Http\Controllers\PostController@store');
//文章详情
Route::get('/posts/{post}','\App\Http\Controllers\PostController@show');
//编辑文章
Route::get('/posts/{post}/edit','\App\Http\Controllers\PostController@edit');
Route::put('/posts/{post}','\App\Http\Controllers\PostController@update');
// 删除文章
Route::get('/posts/{post}/delete','\App\Http\Controllers\PostController@delete');
// 图片上传
Route::post('/posts/image/upload','\App\Http\Controllers\PostController@imageUpload');
//评论文章
Route::post('/posts/{post}/comment','\App\Http\Controllers\PostController@comment');
//赞
Route::get('/posts/{post}/zan','\App\Http\Controllers\PostController@zan');
//取消赞
Route::get('/posts/{post}/unzan','\App\Http\Controllers\PostController@unzan');


//注册页面
Route::get('/register','\App\Http\Controllers\RegisterController@index');
//注册行为
Route::post('/register','\App\Http\Controllers\RegisterController@register');
//登录页面
Route::get('/login','\App\Http\Controllers\LoginController@index');
//登录行为
Route::post('/login','\App\Http\Controllers\LoginController@login');
//退出行为
Route::get('/logout','\App\Http\Controllers\LoginController@logout');
//个人设置页面
Route::get('/user/me/setting','\App\Http\Controllers\UserController@setting');
//个人设置行为
Route::post('/user/me/setting','\App\Http\Controllers\UserController@settingStore');
//个人中心页面
Route::get('/user/{user}','\App\Http\Controllers\UserController@show');
//关注用户
Route::get('/user/{user}/fan','\App\Http\Controllers\UserController@fan');
//取消关注
Route::get('/user/{user}/unfan','\App\Http\Controllers\UserController@unfan');

//专题详情
Route::get('/topic/{topic}','\App\Http\Controllers\TopicController@show');
//投稿
Route::post('/topic/{topic}/submit','\App\Http\Controllers\TopicController@submit');

include_once("admin.php");