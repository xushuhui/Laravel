<?php

Route::group(['prefix' => 'admin'], function() {
    //登录展示页面
    Route::get('/login', '\App\Admin\Controllers\LoginController@index');
    //登录行为
     Route::post('/login', '\App\Admin\Controllers\LoginController@login');
    //退出行为
     Route::get('/logout', '\App\Admin\Controllers\LoginController@logout');

    //后台首页
    Route::get('/home', '\App\Admin\Controllers\HomeController@index');
    //管理人员
    Route::get('/users', '\App\Admin\Controllers\UserController@index');
    Route::get('/users/create', '\App\Admin\Controllers\UserController@create');
    Route::post('/users/store', '\App\Admin\Controllers\UserController@store');
    //文章审核页面
    Route::get('/posts', '\App\Admin\Controllers\PostController@index');
    //文章审核行为
    Route::post('/posts', '\App\Admin\Controllers\PostController@status');

    //专题
    Route::resource('topics', '\App\Admin\Controllers\TopicController',
    ['only' => ['index','create','store','destroy']]);
    

    Route::group(['middleware' => 'auth:admin'], function() {
        //首页

    });
    
});
